## For Project Specific changes following changes are required

1. Inside the bin folder change the file name.
2. Change project name in package.json
3. For recommandations follow this link [https://medium.com/wizardnet972/write-a-simple-node-executable-with-typescript-and-vscode-97c58adca02d](https://medium.com/wizardnet972/write-a-simple-node-executable-with-typescript-and-vscode-97c58adca02d)
4. uninstall jest or express frameworks if your project doesn't required in following steps
    * if you have already install packages then 'npm uninstall --save-dev @types/express @types/jest jest ts-jest' and `npm uninstall express`
    * if you didnt install packages the simply remove following packages names `@types/express,@types/jest,jest,ts-jest,express` from package.json file.

5. Related to Build and run the project follow `3.` point this blog is very usefull