"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express = require("express");
var app = express();
var port = 9000;
app.get('/', function (req, res) {
    res.send("Hello World...!");
});
app.listen(port, function () { return console.log("Server has started"); });
//# sourceMappingURL=index.js.map